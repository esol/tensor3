function matrix = KRproduct(A,B)
% Khatri-Rao product of two matrices
% new = KRproduct(A,B)
% size(A) = I x K
% size(B) = J x K
% size(new) = IJ x K

s=size(A);
t=size(B);

matrix=zeros(s(1)*t(1),s(2));

for j=1:s(2)
for i=1:s(1)
matrix((i-1)*t(1)+1:(i-1)*t(1)+t(1),j) = A(i,j)*B(:,j);

end

end


end
