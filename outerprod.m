function tensor = outerprod(a,b,c)
% dim(a)=I, dim(b)=J, dim(c)=K
% dim(tensor) = I x J x K
I = size(a(:),1);
J = size(b(:),1);
K = size(c(:),1);

tensor = reshape( kron(kron(c(:),b(:))(:), a(:) ) ,[I,J,K]);

%tensor = zeros(I,J,K);

%for i=1:I
%for j=1:J
%for k=1:K
%tensor(i,j,k)=a(i)*b(j)*c(k);

%end
%end
%end

end
