function new = tucker_transform(old, A,B,C)
% new = tucker_transform(old, A , B , C)
% this method is used for packing and unpacking of tensors
% old is a three index tensor
% A, B, C are matrices
% old is contracted with matrices A,B,C to get new


new = contract_tensor3_matrix(old,A,1);
new = contract_tensor3_matrix(new,B,2);
new = contract_tensor3_matrix(new,C,3);

end