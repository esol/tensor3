function [Anew,Bnew,Cnew] = normalize_cp_matrix_columns(A,B,C)
%[Anew,Bnew,Cnew] = normalize_cp_matrix_columns(A,B,C)
% normalizes the matching columns of A, B, C to same number

s = size(A);
Anew = zeros(s);
Bnew = zeros(s);
Cnew = zeros(s);
% number of columns
R = s(2);
    % go through columns
    for i=1:R
        c = 1;
        % norm of a_i
        N = sqrt(sum(A(:,i).^2));
        Anew(:,i) = A(:,i)/N;
        c = c*N;
        % norm of b_i
        N = sqrt(sum(B(:,i).^2));
        Bnew(:,i) = B(:,i)/N;
        c = c*N;
        % norm of c_i
        N = sqrt(sum(C(:,i).^2));
        Cnew(:,i) = C(:,i)/N;
        c = c*N;
        % rescale
        c = cbrt(c);
        Anew(:,i) = c*Anew(:,i);
        Bnew(:,i) = c*Bnew(:,i);
        Cnew(:,i) = c*Cnew(:,i);
    end


end