function new = contract_tensor3_matrix(T,mtx,ind)
% new = contract_tensor3_matrix(T,mtx,ind)
% contract index ind of tensor T with first index of matrix mtx
% T is a three index tensor
% mtx is a matrix
% ind is the index of T to be contracted

st = size(T);
sm = size(mtx);
% st(ind) should equal sm(1)
%assert(st(ind),sm(1))

if ind==1
    % initialize to zero
    %new = zeros(sm(2),st(2),st(3));
    new = permute(T,[2,3,1]);
end 
if ind==2
    %new = zeros(st(1),sm(2),st(3));
    new = permute(T,[1,3,2]);
end
if ind==3
    %new = zeros(st(1),st(2),sm(2));
    new = permute(T,[1,2,3]);
end

% store sizes
sn = size(new);

% reshape tensor to matrix
new = reshape(new,[sn(1)*sn(2),sn(3)]);


% perform matrix multiplication
new = new * mtx;


% reshape back to tensor
new = reshape(new, [sn(1),sn(2),sm(2)]);


% permute according to index
if ind==1
    % initialize to zero
    %new = zeros(sm(2),st(2),st(3));
    new = ipermute(new,[2,3,1]);
end
if ind==2
    %new = zeros(st(1),sm(2),st(3));
    new = ipermute(new,[1,3,2]);
end
if ind==3
    %new = zeros(st(1),st(2),sm(2));
    new = ipermute(new,[1,2,3]);
end


end
