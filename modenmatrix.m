function res = modenmatrix(tensor,n)
% turns tensor to matrix
% size(tensor)= i1 x i2 x i3 x ... x im
% size of resulting matrix:
% in x i1* i2* ...* in-1* in+1* im

    sizes = size(tensor);
    % initialize the resulting matrix
    M=prod(sizes)/sizes(n);
    res = zeros(sizes(n),M);


end
