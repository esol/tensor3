function tensor = tensor_from_matrices(A,B,C)
% tensor = tensor_from_matrices(A,B,C)
% size(A) = I x s
% size(B) = J x s
% size(C) = K x s
% size(tensor) = I x J x K
% no size checks for matrices A,B,C

s=size(A,2);
tensor=zeros(size(A,1),size(B,1),size(C,1));

for i=1:s
tensor = tensor + outerprod(A(:,i),B(:,i),C(:,i));

end



end