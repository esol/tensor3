function [Anew,Bnew,Cnew] = cp_als_m_updates(T1,T2,T3,A,B,C,m)
% perform m iterations of alternating least squares
% [Anew,Bnew,Cnew] = cp_als_m_updates(T1,T2,T3,A,B,C,m)
% T1 is mode-1 matrix of tensor
% T2 is mode-2 matrix of tensor
% T3 is mode-3 matrix of tensor
% A is the x matrix
% B is the y matrix
% C is the z matrix
% m is number of iterations done

Anew = A;
Bnew = B;
Cnew = C;
for i=1:m
    Anew = T1*pinv(KRproduct( Cnew,Bnew ))';
    Bnew = T2*pinv(KRproduct( Cnew,Anew ))';
    Cnew = T3*pinv(KRproduct( Bnew,Anew ))';

end



end
