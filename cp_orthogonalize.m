function [Anew, Bnew, Cnew] = cp_orthogonalize(A,B,C)
% [Anew, Bnew, Cnew] = cp_orthogonalize(A,B,C)
% perform Gram-Schmidt orthogonalization on candecomp matrices A,B,C

s = size(A);
Anew = A;
Bnew = B;
Cnew = C;

for i=1:s(2)-1
  % calculate <u_i|u_i>
  a = sum(Anew(:,i).^2);
  b = sum(Bnew(:,i).^2);
  c = sum(Cnew(:,i).^2);
  unorm = a*b*c;
  uscale = 1;
  % go through other vectors
  for j=i+1:s(2)
    ipx = sum(Anew(:,i).*Anew(j));
    ipy = sum(Bnew(:,i).*Bnew(j));
    ipz = sum(Cnew(:,i).*Cnew(j));
    % inner product <u_i|u_j>
    uvprod = ipx *ipy *ipz;
    scale = cbrt(uvprod/unorm);
    uscale = uscale + scale;
    % remove u_i from u_j
    Anew(:,j) = Anew(:,j) - scale*Anew(:,i);
    Bnew(:,j) = Bnew(:,j) - scale*Bnew(:,i);
    Cnew(:,j) = Cnew(:,j) - scale*Cnew(:,i);
  end

  % scale u_i
  Anew(:,i) = Anew(:,i)*uscale;
  Bnew(:,i) = Bnew(:,i)*uscale;
  Cnew(:,i) = Cnew(:,i)*uscale;

end



end