function matrix = tensor3_to_matrix(tensor,n)
% mode n matrix of rank 3 tensor
% TODO: make one for arbitrary rank tensor


s = size(tensor);

if n==1
matrix=zeros(s(1),s(2)*s(3));
   for i=1:s(1)
   for j=1:s(3)

matrix(i,(j-1)*s(2)+1:(j-1)*s(2)+s(2)) = tensor(i,:,j);

   end
   end


elseif n==2
matrix=zeros(s(2),s(1)*s(3));
   for i=1:s(2)
   for j=1:s(3)

matrix(i,(j-1)*s(1)+1:(j-1)*s(1)+s(1)) = tensor(:,i,j);

   end
   end

else
matrix=zeros(s(3),s(1)*s(2));
   for i=1:s(3)
   for j=1:s(2)

matrix(i,(j-1)*s(1)+1:(j-1)*s(1)+s(1)) = tensor(:,j,i);

   end
   end
end


end
