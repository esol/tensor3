function [Anew,Bnew,Cnew,ers] = cp_als_update_save_errors(T,T1,T2,T3,A,B,C,m,N)
% perform N iterations of alternating least squares
% save Frobenius norm of error residual every m iterations
% [Anew,Bnew,Cnew,ers] = cp_als_update_save_errors(T,T1,T2,T3,A,B,C,m,N)
% inputs:
% T rank 3 tensor
% T1 mode 1 matrix of T
% T2 mode 2 matrix of T
% T3 mode 3 matrix of T
% A x-factor matrix
% B y-factor matrix
% C z-factor matrix
% m save error every m iterations
% N number of iterations done
% outputs:
% Anew, Bnew, Cnew: updated factor matrices
% ers: vector of errors, Frobenius norm of residual tensor

ers=zeros(N/m,1);
counter = 1;
Anew = A;
Bnew = B;
Cnew = C;
for i=1:m:N
    [Anew,Bnew,Cnew] = cp_als_m_updates(T1,T2,T3,Anew,Bnew,Cnew,m);
    % unpack tensor
    Tcomp = tensor_from_matrices(Anew,Bnew,Cnew);
    % calculate Frobenius norm
    ers(counter) = sqrt(sum((T(:)-Tcomp(:)).^2));
    % free memory
    %Tcomp=[];
    % increase counter
    counter = counter + 1;
end


end
