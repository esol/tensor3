function [Anew,Bnew,Cnew,lnew] = cp_rals_m_updates(T1,T2,T3,A,B,C,m,l,q)
% perform regularized alternating least squares
% [Anew,Bnew,Cnew,lnew] = cp_rals_m_updates(T1,T2,T3,A,B,C,m,l,q)
% T1 mode 1 matrix of tensor T
% T2 mode 2
% T3 mode 3
% A x-factor matrix
% B y-factor matrix
% C z-factor matrix
% m number of iterations done
% l regularization parameter
% q scaling parameter for regularization


Anew = A;
Bnew = B;
Cnew = C;
lnew = l;
% rank R of A,B, and C
R = size(A,2);

% iterate
for i=1:m

W = [KRproduct(Cnew,Bnew);lnew*eye(R)];
S = [T1,lnew*Anew];
Anew = S*pinv(W)';

W = [KRproduct(Cnew,Anew);lnew*eye(R)];
S = [T2,lnew*Bnew];
Bnew = S*pinv(W)';

W = [KRproduct(Bnew,Anew);lnew*eye(R)];
S = [T3,lnew*Cnew];
Cnew= S*pinv(W)';

% update regularization parameter lnew
lnew = lnew*q;

end


end
